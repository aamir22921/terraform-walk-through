# Terraform walk-through

## Understanding Attributes and Output Values in Terraform

* [`aws_eip`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip#attributes-reference).
* [`aws_s3_bucket`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#attributes-reference)

1. Example folder is [`terraform/aws_attribute`](https://gitlab.com/aamir22921/terraform-walk-through/-/tree/main/terraform/aws_attribute).

## Referencing Cross-Account Resource Attributes

* [`aws_eip_association`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip_association)
* [`aws_security_group`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)

1. Example 1: - Elastic IP address attach to the EC2 instance.
2. Example 2: - Elastic IP get reference in the security group.
3. Example folder is [`terraform/aws_eip_association`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_eip_association/eip_ec2_sg.tf).

## Terraform Variables

1. Example folder is [`terraform/aws_variables_demo`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_variables_demo/varsdemo.tf)

## Approaches of Variable Assignments

### custom.tfvars

```sh
instancetype="t2.large"
```

### CLI Commands

```sh
terraform plan -var="instancetype=t2.small"
terraform plan -var-file="custom.tfvars"
```

### Windows Approach

```sh
setx TF_VAR_instancetype t2.large
```

### Linux / MAC Approach

```sh
export TF_VAR_instancetype="t2.nano"
echo $TF_VAR
```

1. Example folder is [`terraform/aws_ec2_tfvars`](https://gitlab.com/aamir22921/terraform-walk-through/-/tree/main/terraform/aws_ec2_tfvars)

## Datatypes of variables

1. Example of datatypes of variables in Terraform [`terraform/aws_iamuser_datatype`](https://gitlab.com/aamir22921/terraform-walk-through/-/tree/main/terraform/aws_iamuser_datatype)

2. Example of [`terraform-aws-modules/terraform-aws-elb`](https://github.com/terraform-aws-modules/terraform-aws-elb/blob/master/variables.tf)

## Fetching Data for Maps and List in Variable

1. Example of fetching data [`terraform/aws_fetching_variables`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_fetching_variables/ec2_fetching_instancetype.tf)

## Count and Count Index

1. Example of count and count index [`terraform/aws_ec2_count_parameter`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_ec2_count_parameter/count-index.tf)

## Conditional Expression

1. Example of conditional expression [`terraform/aws_ec2_conditional`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_ec2_conditional/ec2_conditional.tf)

## Local Values

1. Terraform documentation about [`Locals`](https://www.terraform.io/docs/language/values/locals.html)

2. Example of local values [`terraform/aws_ec2_locals_common`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_ec2_locals_common/ec2_locals.tf)

## Terraform Functions

1. Terraform documentation about [`lookup Functions`](https://www.terraform.io/docs/language/functions/lookup.html)

2. Terraform documentation about [`element Functions`](https://www.terraform.io/docs/language/functions/element.html)

3. Terraform documentation about [`timestamp Functions`](https://www.terraform.io/docs/language/functions/timestamp.html)

4. Terraform documentation about [`formatdate Functions`](https://www.terraform.io/docs/language/functions/formatdate.html)

5. Terraform documentation about [`file Functions`](https://www.terraform.io/docs/language/functions/file.html)

6. Example of terraform functions [`terraform/aws_terraform_functions`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_terraform_functions/built_in_functions.tf)

## Data Sources

1. Terraform documentation about [`Data Sources`](https://www.terraform.io/docs/language/data-sources/index.html)

2. Example of terraform datasources [`terraform/aws_datasource`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_datasource/ec2_datasource.tf)

## Debugging in Terraform

* `TRACE` is the most verbose and it is the default if `TF_LOG` is set to something other than a log level name

```sh
export TF_LOG = TRACE
```

* To persist logged output you can set `TF_LOG_PATH` in order to force the log to always be appended to a specific file when logging is enabled.

```sh
export TF_LOG_PATH = /tmp/terraform-crash.log
```

## Validation Terraform Configuration Files

### Command for Validating

```sh
terraform validate
```

## Load Order and Semantics

### Understanding Semantics

* Terraform generally loads all the configuration files within the directory specified in alphabetical order.

* The files loaded must end in either .tf or .tf.json to specify the format that is in use.

## Dynamic Blocks

* Dynamic Block allows us to dynamically construct repeatable nested blocks which is supported inside resource, data, provider and provisioner blocks.

* The iterator argument(optional) sets the name of a temporary variable that represents the current element of the complex value.

* Example of terraform dynamicblocks [`terraform/aws_dynamic_blocks`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_dynamic_blocks/dynamic-block.tf)

## Tainting Resources

### Overview of Terraform Taint

* The terraform taint command manually marks a Terraform-managed resource as `tainted`, forcing it to be destroyed and recreated on the next apply.

* The command will not modify infastructure, but does modify the state file in order to mark a resource as tainted.

* Once a resource is marked as tainted, the next plan will show that the resource will be destroyed and recreated and the next apply will implement this change.

* Note that tainting a resource for recreation may affect resources that depend on the newly tainted resource.

### Taint Command

```sh
terraform taint aws_instance.myec2
```

### Spalat Expression

* Splat Expression allows us to get a list of all the attributes.

* Example of terraform spalat [`terraform/aws_spalat_expression`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_spalat_expression/spalat.tf)

## Terraform Graph

* The terraform graph command is used to generate a visual representation of exither a confiiguration or execution plan.

* The output of terraform graph is in the DOT format, which can easily be converted to an image.

### Commands Used

```sh
terraform graph > graph.dot
yum install graphviz
cat graph.dot | dot -Tsvg > graph.svg
```

## Saving Terraform Plan to file

### Commands used

```sh
terraform plan -out=demopath
terraform apply demopath
```

## Terraform Output

* The terraform output command is used to extract the value of an output variable from the state file.

```sh
terraform output iam_names
```

## Terraform Settings

* Example of terraform settings [`terraform/aws_tf_settings`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_tf_settings/ec2_tf_settings.tf)

## Dealing with Large Infrastructure

### Setting Refresh as False

```sh
terraform plan -refresh=false
```

### Setting Refresh along with Target flags

```sh
terraform plan -refresh=false -target=aws_security_group.allow_ssh_conn
```

## Zipmap

* Example of terraform zipmap [`terraform/aws_zipmap`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/aws_zipmap/zipmap.tf)

## Terraform Provisioners

* Provisioners are used to execute scripts on a local or remote machine as part of resource creation or destruction.

* local-exec provisioners allows us to invoke a local executable after the resource is created.

* One of the most used approach of local-exec is to run ansible-playbooks on the created server after the resource is created.

* Example of terraform remote-exec [`terraform/provisioners/remote-exec`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/provisioners/remote-exec/ec2.tf)

* Example of terraform local-exec [`terraform/provisioners/local-exec`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/provisioners/local-exec/ec2.tf)

* `Creation-Time Provisioner` are only run during creation, not during updating or any other lifecycle.`If a creation-time provisioner fails, the resource is marked as tainted`.

* `Destroy-Time Provisioner` are run before the resource is destroyed.

* Example of terraform provisioner-types [`terraform/provisioners/provisioner-types`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/provisioners/provisioner-types/ec2.tf)

* Example of terraform provisioner-failure-types [`terraform/provisioners/provisioner-failure-types`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/provisioners/provisioner-failure-types/ec2.tf)

* By default, provisioners that fail will also cause the terraform apply itself to fail.

* The on_failure setting can be used to change this. The allowed values are:

 1. continue :- Ignore the error and continue with creation or destruction

 2. fail :- Raise an error and stop applying (the default behavior). If this is a creation provisioner, taint the resource.

## Modules

* Example of terraform module [`terraform/modules`](https://gitlab.com/aamir22921/terraform-walk-through/-/tree/main/terraform/modules)

* Example of terraform module-registry [`terraform/module-registry`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/modules/module-registry/registry_module_ec2.tf)

## Workspace

```sh
terraform workspace -h
terraform workspace show
terraform workspace new dev
terraform workspace new prd
terraform workspace list
terraform workspace select dev
```

* Example of terraform workspace [`terraform/workspace/aws_ec2`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/workspace/aws_ec2/ec2.tf)

## Module Sources

* Example of terraform module sources [`terraform/module_sources/aws_ec2`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/module_sources/aws_ec2/module_sources.tf)

* Terraform documentation about [`Module Sources`](https://www.terraform.io/docs/modules/sources.html#github)

## Remote backend with S3 and Dynamodb lock

* Example of terraform with S3 backend without lock [`terraform/remote-backend/aws_ec2`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/remote-backend/aws_ec2/backend.tf)

* Example of terraform with S3 backend with lock [`terraform/remote-backend_with_lock`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/remote-backend_with_lock/backend.tf)

## State Management Commands

### List the Resource with State File

```sh
terraform state list
```

#### Rename Resource within Terraform State

1. Change the local name of EC2 resource from webapp to myec2.
2. Run terraform plan to see the changes. It should destroy and recreate the EC2 instance.
3. Change the local name of EC2 within the terraform state file with following command:

```sh
terraform state mv aws_instance.myec2 aws_instance.webapp
```

#### Pull Current State file

```sh
terraform state pull
```

#### Remove Items from State file

```sh
terraform state rm aws_instance.myec2 
```

#### Show Attributes of Resource from state file

```sh
terraform state show aws_iam_user.lb
```

## Command to import resource

```sh
terraform import aws_instance.myec2 i-041886ebb7e9bd20
```

## Multiple providers with alias

* Example of terraform with multiple providers [`terraform/multiple_providers_using_alias`](https://gitlab.com/aamir22921/terraform-walk-through/-/blob/main/terraform/multiple_providers_using_alias/ec2.tf)

## Assume Role STS

* Example of terraform documentation[`terraform assume role`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#assume-role)

## Sentinel Resource

* Example of terraform documentation [`Sentinel Resource`](https://docs.hashicorp.com/sentinel/terraform/)