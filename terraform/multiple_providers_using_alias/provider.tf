provider "aws" {
  alias = "north-virginia"
  region = "us-east-1"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}

provider "aws" {
  alias = "north-california"
  region = "us-west-1"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}