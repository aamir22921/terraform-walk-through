resource "aws_instance" "myec2" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = "t2.micro"
   key_name = "aws_key_pair"
   vpc_security_group_ids = [aws_security_group.dynamic_sg.id]

   provisioner "remote-exec" {
     inline = [
       "sudo yum -y install nano"
     ]
   }
    provisioner "remote-exec" {
     when = destroy
     inline = [
       "sudo yum -y remove nano"
     ]
   }

   connection {
     type = "ssh"
     user = "ec2-user"
     private_key = file("./aws_key_pair.pem")
     host = self.public_ip
   }
   
}

resource "aws_security_group" "dynamic_sg" {
  name        = "dynamic-sg"
  description = "Ingress and Egress rule"
  dynamic "ingress"{
     for_each = var.sg_ports
     iterator = port
     content{
    from_port        = port.value
    to_port          = port.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
     }
  }
  /* By default, AWS creates an ALLOW ALL egress rule when creating a new Security Group inside of a VPC. When creating a new Security Group inside a VPC, 
  Terraform will remove this default rule, and require you specifically re-create it if you desire that rule. We feel this leads to fewer surprises in terms of controlling your egress rules. 
  If you desire this rule to be in place, you can use this egress block:
  */
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

   

