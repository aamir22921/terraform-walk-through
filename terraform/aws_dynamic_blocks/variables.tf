variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}

variable "sg_ports"{
  type = list(number)
  description = "list of ingress and egress ports"
  default = [8200,8201,8300,9200,9500]
}