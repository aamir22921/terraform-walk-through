terraform {
  backend "s3" {
    bucket = "aamir22921-remote-backend"
    key    = "demo1.tfstate"
    region = "us-east-1"
    access_key = "YOUR_ACCESS_KEY_HERE"
    secret_key = "YOUR_SECRET_KEY_HERE"
  }
}

