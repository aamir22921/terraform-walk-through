variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}
variable "usernumber"{
  type = number
  default = 48503
}

variable "elb_name"{
  type = string
  default = "ELB-demo"
}

variable "az" {
  type = list
  default =["us-east-1a"]
}

variable "timeout" {
  type = number
  default = 400
}