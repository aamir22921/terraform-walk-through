module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "my-cluster"
  instance_count         = 1

  ami                    = "ami-0ab4d1e9cf9a1215a"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-0cbb8a2f722232269"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}