
# Fetching through type list
resource "aws_instance" "myec2" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = var.list[0]
}
# Fetching through type map
resource "aws_instance" "myec21" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = var.map["us-east-1"]
}
