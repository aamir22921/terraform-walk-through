# Example 1: Generic GIT Repository

module "demomodule" {
  source = "git::https://github.com/zealvora/tmp-repo.git"
}

# Example 2: Generic GIT Repository with Specific branch

module "demomodule1" {
  source = "git::https://github.com/zealvora/tmp-repo.git?ref=development"
}

# Example 3: GitHub Source

module "demomodule2" {
  source = "github.com/zealvora/tmp-repo"
}