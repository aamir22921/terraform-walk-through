resource "aws_instance" "dev" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = "t2.micro"
   count = var.is-test == true ? 1 : 0
}

resource "aws_instance" "prod" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = "t2.large"
   count = var.is-test == false ? 1 : 0
}