variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}
variable "region"{
  default = "us-east-1"
}
variable "ami"{
  type = map
  default = {
    "us-east-1" = "ami-0323c3dd2da7fb37d"
    "us-west-2" = "ami-0d6621c01e8c2de2c"
    "ap-south-1" = "ami-0470e33cd681b2476"
  }
}
variable "tags"{
  type = list 
  default = ["firstec2","secondec2"]
}