locals {
   time = formatdate("DD MM YYYY hh:mm ZZZ", timestamp())
}
resource "aws_instance" "apps-dev"{
   ami = lookup(var.ami,var.region)
   instance_type = "t2.micro"
   count = 2
   tags = {
      Name = element(var.tags,count.index)
   }
}
output "timestamp"{
   value = local.time
}