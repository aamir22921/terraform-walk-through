resource "aws_s3_bucket" "b" {
  bucket = "aamir22921-remote-backend"
  acl    = "private"
  versioning {
    enabled = true
  }
  lifecycle {
      prevent_destroy = false
  }
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}