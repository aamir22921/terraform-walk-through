variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}
variable "vpc_variable" {
  default = "169.25.30.4/32"
}