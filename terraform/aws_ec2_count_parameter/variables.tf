variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}

variable "elb_names" {
  type = list
  default = ["dev-loadbalancer","stage-loadbalancer","prod-loadbalancer"]
}