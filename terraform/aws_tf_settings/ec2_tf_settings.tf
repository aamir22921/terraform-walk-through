# Terraform version settings for tf version and providers
terraform {
  #required_version = "< 0.11"
  required_providers {
    aws = "~> 2.0"
  }
}
resource "aws_instance" "myec2" {
   ami = "ami-0aeeebd8d2ab47354"
   instance_type = "t2.micro"
}