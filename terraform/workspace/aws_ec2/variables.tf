variable "aws_secret_access_key"{
  type = string
  sensitive = true
}
variable "aws_access_key_id"{
  type = string
  sensitive = true
}

variable "instance_type" {
  type = map
  default = {
    default = "t2.nano"
    prod = "t2.large"
    dev = "t2.micro"
  }
}